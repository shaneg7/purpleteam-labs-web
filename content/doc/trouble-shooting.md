---
title: Trouble Shooting
linktitle: Trouble Shooting
summary: Problems people have come across with solutions or workarounds.
toc: true
type: book
date: "2021-05-08T00:00:00+01:00"
draft: false

# Prev/next pager order (if `docs_section_pager` enabled in `params.toml`)
weight: 130
---

<br>If any of these issues appear to be happening in the `cloud` environment, contact BinaryMist with details.<br><br><br>

## SUT requests taking too long

### Description

When running _purpleteam_, the time of the web requests to your _SUT_ seem to be taking longer than they should be.

### Cause

This can be due to scripts in your _SUT_ that are taking a long time to load or not loading at all. We saw this initially with NodeGoat in regards to _SUT_ pages attempting to fetch the livereload script. NodeGoat was expecting the livereload script to be hosted locally which it wasn't, subsequently the page load wouldn't finish loading.

### Solution

Check that Zap doesn't have any "Timed out while reading a new HTTP request" messages. If it does:

1. Debug the relevant app-scanner cucumber step where the Selenium webdriver instance makes it's requests to your _SUT_
2. Then VNC into the selenium container, open the browser tools and check that all resources are in-fact loading in a timely manner

We fixed this by removing the dependency on this script (livereload.js) in the NodeGoat `production` environment.<br><br><br>

## URL not found in the Scan Tree

### Description

App test failing (specifically the Zap active scan) with the following error message displayed in the CLI app log:  
`URL Not Found in the Scan Tree`.  
This error is also visible in the app-scanner log and originates from Zap. Zap also logs the following message as a `WARN` event:  
`Bad request to API endpoint [/JSON/ascan/action/scan/]`  
`URL Not Found in the Scan Tree`

### Cause

This can be due to one or more missing `attackFields` in the Build User config (Job) for a given `route` that you have specified. These `attackFields` are not only used by Selenium to proxy the specific `route`'s request through Zap, but also used to inform Zap of the `postData` when a request is made to [ascanActionScan](https://www.zaproxy.org/docs/api/?java#ascanactionscan).

### Solution

Check that your Build User config (Job) contains all of the `attackFields` that your _SUT_ requires to make a successful request.<br><br><br>

## "Terminfo parse error" in Terminal

### Description

Running the _PurpleTeam_ CLI may produce a `Warning` message: `Terminfo parse error`. Alternatively you may be forced to use Putty to SSH to the Linux host running the PurpleTeam CLI.

### Cause

This is due to the `TERM` environment variable being incompatible for the CLI dependency blessed.

### Solution

Try setting the `TERM` environment variable to something other than your system default before running the _PurpleTeam_ CLI. Blessed-contrib provided [some details](https://github.com/yaronn/blessed-contrib/#troubleshooting).

We have had good results with `TERM=xterm`.

In the case of Putty you can [set your `TERM`](https://askubuntu.com/questions/2995/how-is-the-default-term-shell-variable-value-set) under Connection->Data there is a setting called terminal-type string. You can set your `TERM` there and putty instructs SSH to set that environment variable.

