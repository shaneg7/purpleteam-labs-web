+++
# A Demo section created with the Blank widget.
# Any elements can be added in the body: https://wowchemy.com/docs/writing-markdown-latex/
# Add more sections by duplicating this file and customizing to your requirements.

widget = "blank"  # See https://wowchemy.com/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 70  # Order that this section will appear.

title = "Plans"
subtitle = "<br>From solo Engineers to managing many commercial Engineering Teams, we have the right plan for you.<br><br>"

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "navy"
  
  # Background gradient.
  #gradient_start = "DarkGreen"
  #gradient_end = "ForestGreen"
  
  # Background image.
  # image = "image.jpg"  # Name of image in `static/media/`.
  # image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.
  # image_size = "cover"  #  Options are `cover` (default), `contain`, or `actual` size.
  # image_position = "center"  # Options include `left`, `center` (default), or `right`.
  # image_parallax = true  # Use a fun parallax-like fixed background effect? true/false
  
  # Text color (true=light or false=dark).
  #text_color_light = true

[design.spacing]
  # Customize the section spacing. Order is top, right, bottom, left.
  padding = ["100px", "0", "100px", "0"]

[advanced]
 # Custom CSS. 
 css_style = ""
 
 # CSS class.
 css_class = "pt-home-plans"
+++

<!-- copied and adapted from pricing page -->

<div class="container text-center">
  <div class="row">
    <div class="col-xs-1 col-md-6 col-lg-6 col-xl-3">
      <div class="card" style="min-height: initial;">
        <!--<img src="owasp.png" class="card-img-top" alt="free forever">-->
        {{< figure src="pricing/PT_plan_owasp.svg" theme="light" alt="free forever" width="576" >}}
        <div class="card-body" style="flex-grow: 0;">
          <h3 class="card-title" style="margin-top: 0;">Free</h3>
        </div>
        <ul class="list-group list-group-flush pt-price">
          <li class="list-group-item card-title">
            <small><br>All the features hosted locally</small>
            <small><br>Setup and maintenance required.</small>
          </li>
        </ul>
        <div class="card-body">
          <a href="/doc/local/" class="btn btn-danger btn-block">Get Started</a>
        </div>
      </div>
    </div>
    <div class="col-xs-1 col-md-6 col-lg-6 col-xl-3">
      <div class="card" style="min-height: initial;">
        <!--<img src="blank400x200.png" class="card-img-top" alt="Standard Pricing">-->
        {{< figure src="pricing/PT_plan_standard.svg" theme="light" alt="Standard Pricing" width="576" >}}
        <div class="card-body" style="flex-grow: 0;">
          <h3 class="card-title" style="margin-top: 0;">Standard</h3>
        </div>
        <ul class="list-group list-group-flush pt-price pt-standard">
          <li class="list-group-item card-title">
            <small><br>For busy growing teams that</small>
            <small><br>realise the need for security.</small>
          </li>
        </ul>
        <div class="card-body">
          <a href="/pricing/" class="btn btn-block pt-cta-btn-standard">Learn More</a>
        </div>
      </div>
    </div>
    <div class="col-xs-1 col-md-6 col-lg-6 col-xl-3">
      <div class="card" style="min-height: initial;">
        <!--<img src="blank400x200.png" class="card-img-top" alt="Pro Pricing">-->
        {{< figure src="pricing/PT_plan_pro.svg" theme="light" alt="Pro Pricing" width="576" >}}
        <div class="card-body" style="flex-grow: 0;">
          <h3 class="card-title" style="margin-top: 0;">Pro</h3>
        </div>
        <ul class="list-group list-group-flush pt-price pt-pro">
          <li class="list-group-item card-title">
            <small><br>For mature teams</small>
            <small><br>that need a bit more.</small>
          </li>
        </ul>
        <div class="card-body">
          <a href="/pricing/" class="btn btn-primary btn-block">Learn More</a>
        </div>
      </div>
    </div>
    <div class="col-xs-1 col-md-6 col-lg-6 col-xl-3">
      <div class="card" style="min-height: initial;">
        <!--<img src="blank400x200.png" class="card-img-top" alt="Custom Pricing">-->
        {{< figure src="pricing/PT_plan_enterprise.svg" theme="light" alt="Custom Pricing" width="576" >}}
        <div class="card-body" style="flex-grow: 0;">
          <h3 class="card-title" style="margin-top: 0;">Enterprise</h3>
        </div>
        <ul class="list-group list-group-flush pt-price">
          <li class="list-group-item card-title">
            <small><br>A custom plan for</small>
            <small><br>enterprises with many teams.</small>
          </li>
        </ul>
        <div class="card-body">
          <a href="/contact/" class="btn btn-block btn-dark pt-cta-btn-enterprise">Let's Talk</a>
        </div>
      </div>
    </div>
  </div>
</div>
