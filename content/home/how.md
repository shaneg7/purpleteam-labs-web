---
# An instance of the Featurette widget.
# Documentation: https://wowchemy.com/docs/page-builder/
widget: featurette

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 30

title: How is PurpleTeam Different?
subtitle: <br>We cover your engineering stack. We don't depend on specific integrations<br>we just give you a process (CLI) to run, simple, run it anywhere.

# Showcase personal skills or business features.
# - Add/remove as many `feature` blocks below as you like.
# - For available icons, see: https://wowchemy.com/docs/page-builder/#icons
feature:
- description: You can dial the attack strength to mitigate false positives
  icon: tachometer-alt
  icon_pack: fas
  name: Attack Strength
- description: Great for brown field projects with existing security defects
  icon: exclamation-triangle
  icon_pack: fas
  name: Alert Thresholds
- description: Run the purpleteam process anywhere Toggle headless mode
  icon: running
  icon_pack: fas
  name: Run Anywhere
- description: Built from ground up to support Development Teams
  icon: laptop-code
  icon_pack: fas
  name: Developers First


# Uncomment to use emoji icons.
#- icon = ":smile:"
#  icon_pack = "emoji"
#  name = "Emojiness"
#  description = "100%"  

# Uncomment to use custom SVG icons.
# Place custom SVG icon in `assets/images/icon-pack/`, creating folders if necessary.
# Reference the SVG icon name (without `.svg` extension) in the `icon` field.
#- icon = "your-custom-icon-name"
#  icon_pack = "custom"
#  name = "Surfing"
#  description = "90%"

advanced:
  # Custom CSS.
  css_style: ""

  # CSS class.
  css_class: "pt-featurette-col-3333"
---
